FROM golang
MAINTAINER Matt Williams

RUN go get github.com/paulbellamy/ratecounter
RUN go get github.com/garyburd/redigo/redis
ADD dd-demo /go/src/dd-demo
RUN go install dd-demo
ENTRYPOINT /go/bin/dd-demo
EXPOSE 8000
